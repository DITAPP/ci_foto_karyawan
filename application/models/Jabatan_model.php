<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan_model extends CI_Model
{
	//panggil nama table
	private $_table = "jabatan";
	
	public function tampilDataJabatan()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	
	}
	
	public function rules()
	{
		return [
			[
			
				'field' => 'kode_jabatan',
				'label' => 'Kode jabatan',
				'rules' => 'required|max_length[5]',
				'errors' => [
					'required' => 'Kode jabatan Tidak Boleh Kosong.',
					'max_length'=> 'Kode jabatan Tidak Boleh Lebih Dari 5 Karakter.',
				],
			],
			[
				'field' => 'nama_jabatan',
				'label' => 'Nama jabatan',
				'rules' => 'required',
				'errors' => [
					'required' => 'Nama jabatan Tidak Boleh Kosong.',
				],
			],
			[
				'field' => 'keterangan',
				'label' => 'keterangan',
				'rules' => 'required',
				'errors' => [
					'required' => 'keterangan Tidak Boleh Kosong.',
				],
			]
			//[
			//	'field' => 'stok',
			//	'label' => 'Stok Barang',
			//	'rules' => 'required|numeric',
			//	'errors' => [
			//		'required' => 'Stok Barang Tidak Boleh Kosong.',
			//		'numeric' => 'Stok Barang Harus Angka.',
			//	],
			//]
		];	
	}

	public function tampilDataJabatan2()
	{
		$query = $this->db->query("SELECT * FROM jabatan WHERE flag = 1");
		return $query->result();
	
	}
	
	public function tampilDataJabatan3()
	{
		$this->db->select('*');
		$this->db->order_by('kode_jabatan', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function save()
	{	
		$data['kode_jabatan']	=$this->input->post('kode_jabatan');
		$data['nama_jabatan']	=$this->input->post('nama_jabatan');
		$data['keterangan']		=$this->input->post('keterangan');
		$data['flag']			=1;
		$this->db->insert($this->_table, $data);
	}
	
	public function detail($kode_jabatan)
	{
		$this->db->select('*');
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function edit($kode_jabatan)
	{
		$this->db->select('*');
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function update($kode_jabatan)
	{
		$data['kode_jabatan']	=$this->input->post('kode_jabatan');
		$data['nama_jabatan']	=$this->input->post('nama_jabatan');
		$data['keterangan']		=$this->input->post('keterangan');
		$data['flag']			=1;
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->update($this->_table, $data);
	}
	
	public function delete ($kode_jabatan)
	{
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->delete($this->_table);	
	}



}
