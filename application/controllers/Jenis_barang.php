<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_barang extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("jenis_barang_model");

		$this->load->library('form_validation');
	}
	
	public function index()
	{
		$this->listjenisbarang();
	}
	
	public function listjenisbarang()
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenisBarang();
		$data['content'] = 'forms/list_jenis_barang';
		$this->load->view('home_', $data);
	}
	
	public function inputjenisbarang()
	
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenisBarang();
		//if (!empty($_REQUEST)) {
		//	$m_jenis_barang = $this->jenis_barang_model;
		//	$m_jenis_barang->save();
		//	redirect("jenis_barang/index", "refresh");
		//}

		//validasi terlebih dahulu
		$validation = $this->form_validation;
		$validation->set_rules($this->jenis_barang_model->rules());
		
		if ($validation->run()){
			$this->jenis_barang_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil</div>');
			redirect("jenis_barang/index", "refresh");
			}
			
		$data['content'] ='forms/input_jenis_barang';
		$this->load->view('home_', $data);
			
		//$this->load->view('input_jenis_barang');
	}
	public function detailjenisbarang($kode_jenis)
	
	{
		$data['detail_jenis_barang'] = $this->jenis_barang_model->detail($kode_jenis);
		$this->load->view('detail_jenis_barang', $data);
	}
	
	
	public function editjenisbarang($kode_jenis)
	{
		$data['edit_jenis_barang'] = $this->jenis_barang_model->tampilDataJenisBarang();
		
		//if(!empty($_REQUEST)) {
		//	$m_jenis_barang = $this->jenis_barang_model;
		//	$m_jenis_barang->update($kode_jenis);
		//	redirect("jenis_barang/index", "refresh");
		//	}
		
		//$this->load->view('edit_jenis_barang', $data);	

		$validation = $this->form_validation;
		$validation->set_rules($this->jenis_barang_model->rules());
		
		if ($validation->run()){
			$this->jenis_barang_model->update($kode_jenis);
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil</div>');
			redirect("jenis_barang/index", "refresh");
			}
			
		$data['content'] ='forms/edit_jenis_barang';
		$this->load->view('home_', $data);	
	}
	
	public function deletejenisbarang($kode_jenis)
	{
			$m_jenis_barang = $this->jenis_barang_model;
			$m_jenis_barang->delete($kode_jenis);
			redirect("jenis_barang/index", "refresh");
	}

	
}