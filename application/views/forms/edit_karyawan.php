<!--<table width="100%" height="54" border="1">
  <tr>
    <td height="23" colspan="6" align="center" bgcolor="#3399FF"><b>TOKO JAYA ABADI</b></td>
  </tr>
  <tr bgcolor="#33CCFF">
    <td height="23" align="center"><li><a href="<?=base_url();?>main">Home</a></li></td>
    <td align="center"><li><a href="<?=base_url();?>karyawan/listkaryawan">Master</a></li></td>
    <td align="center"><li><a href="<?=base_url();?>jabatan/listjabatan">Transaksi</a></li></td>
    <td align="center"><li><a href="<?=base_url();?>barang/listbarang">Report</a></li></td>
    <td align="center"><li><a href="<?=base_url();?>jenis_barang/listjenisbarang">Logout</a></li></td>
  </table> -->
 
<div align="center" >
  <h1>Edit Karyawan</h1></div>
  
  <?php
	foreach($edit_karyawan as $data) {
		
		
		$nik          =$data->nik;
		$nama_lengkap	=$data->nama_lengkap;
		$tempat_lahir	=$data->tempat_lahir;
		$tgl_lahir		=$data->tgl_lahir;
		$jenis_kelamin=$data->jenis_kelamin;
		$alamat			  =$data->alamat;
		$telp			    =$data->telp;
		$kode_jabatan	=$data->kode_jabatan;
    $foto         =$data->foto;
		
	}
	$thn_pisah = substr($tgl_lahir, 0, 4);
	$bln_pisah = substr($tgl_lahir, 5, 2);
	$tgl_pisah = substr($tgl_lahir, 8, 2);
?>
  
<form action="<?=base_url()?>karyawan/editkaryawan/<?=$nik?>" method="POST" enctype="multipart/form-data">
<table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" >
<center>
  <?= validation_errors();?>
</center>
  <tr>
    <td width="37%">NIK</td>
    <td width="4%"> :</td>
    <td width="59%">
      <input type="text" name="nik" id="nik" maxlength="50" value="<?=$nik;?>" >
    </td>
  </tr>
  <tr>
    <td>Nama </td>
    <td>:</td>
    <td>
      <input type="text" name="nama" id="nama" maxlength="50" value="<?=$nama_lengkap;?>">
    </td>
  </tr>
  <tr>
    <td width="37%">Tempat Lahir</td>
    <td width="4%"> :</td>
    <td width="59%">
      <input type="text" name="tempatlahir" id="tempatlahir" maxlength="50" value="<?=$tempat_lahir;?>">
    </td>
  </tr>
  <tr>
    <td>Jenis Kelamin</td>
    <td>:</td>
    <td>
      <select name="jenis_kelamin" id="jenis_kelamin" value="<?=$jenis_kelamin;?>">
      <?php
	   $jk = ['L','P'];
	   $jk_text = ['L' => 'Laki-Laki', 'P' => 'Perempuan'];
	   
      	foreach ($jk as $data ) {
			if($data == $jenis_kelamin) {
				$select = 'selected';
				} else {
					$select = '';
					}
	  ?>
      	<option value="<?= $data;?>" <?= $select;?>> <?=$jk_text[$data]; ?></option>
        <?php }?>
      </select>
   </td>
  </tr>
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td>
      <select name="tgl" id="tgl" >
      <?php
      	for($tgl=1;$tgl<=31;$tgl++){
			$select_tgl = ($tgl == $tgl_pisah)?'selected' : '' ;
		?>
        
      	<option value="<?=$tgl;?>" <?=$select_tgl;?>> <?=$tgl;?></option>
       <?php
       	}
       ?>
      </select>
      
      <select name="bln" id="bln">
      <?php
      	$bulan_n = array('Januari','Februari','Maret','April',
        				'Mei','Juni','Juli','Agustus','September',
                        'Oktober','November','Desember');
         for($bln=0;$bln<12;$bln++){
			 $select_bln = ($bln == $bln_pisah)?'selected' : '' ;
      ?>
        	<option value="<?=$bln+1;?>" <?=$select_bln;?>> <?=$bln;?></option>
        <?php
        	}
        ?>
      </select>
      
      <select name="thn" id="thn">
      <?php
      	for($thn = date('Y')-60;$thn<=date('Y')-15;$thn++){
			$select_thn = ($thn == $thn_pisah)?'selected' : '' ;
      ?>
        <option value="<?=$thn;?>" <?=$select_thn;?>> <?=$thn;?></option>
      <?php
      	} 
      ?>
      </select>
    </td>
  </tr>
 <tr>
    <td width="37%">Telepon</td>
    <td width="4%"> :</td>
    <td width="59%">
      <input type="text" name="telp" id="telp" maxlength="50" value="<?=$telp;?>">
    </td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td>
      <textarea name="alamat" id="alamat" cols="45" rows="5" value="<?=$alamat;?>"></textarea>
   </td>
  </tr>
  <tr>
    <td>Jabatan</td>
    <td>:</td>
    <td>
      <select name="kode_jabatan" id="kode_jabatan">
      	<?php foreach($data_jabatan as $data){
			$select_jabatan = ($data->kode_jabatan ==
			$kode_jabatan) ? 'selected' : '' ;
		?>
        <option value="<?= $data->kode_jabatan;?>"
        <?= $select_jabatan; ?> > <?=$data->nama_jabatan;?> </option>
        <?php }
		?>
        </select>
    </td>
  </tr>
  <tr>
    <td style="text-align: left;">Upload Foto</td>
    <td>:</td>
    <td style="text-align: center;">
      <input type="file" name="image" id="image">
      <input type="hidden" name="foto_old" id="foto_old" value="<?=$foto;?>" >
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
      <input type="submit" name="Submit" id="Submit" value="Simpan">
      <input type="reset" name="Batal" id="Batal" value="Batal">
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
    	<a href="<?=base_url();?>karyawan/listkaryawan">
      <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"> </a>
    </td>
  </tr>
</table>
</form>
